<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewController extends AbstractController
{
    #[Route('/')]
    public function noHome(): RedirectResponse
    {
        return $this->redirectToRoute('app_cerema');
    }

    #[Route('/bootstrap', name: 'app_bootstrap')]
    public function bootstrap(): Response
    {
        return $this->render('view/bootstrap.html.twig', [
            'controller_name' => 'ViewController',
        ]);
    }

    #[Route('/cerema', name: 'app_cerema')]
    #[Route('/cerema/{theme}', name: 'app_cerema_theme')]
    public function cerema(string $theme = 'cerema' ): Response
    {
        $complements = ['', '-400', '-300', '-200', '-100'];
        $couleurs = ['cerema', 'batiment', 'environnement', 'expertise', 'infrastructure', 'mer', 'mobilite'];
        $theme = match ($theme) {
            'batiment', 'environnement', 'expertise', 'infrastructure', 'mer', 'mobilite' => 'cerema-' . $theme,
            default => 'cerema',
        };

        return $this->render("view/$theme.html.twig", [
            'complements' => $complements,
            'couleurs' => $couleurs,
            'theme' => $theme,
        ]);
    }
}
