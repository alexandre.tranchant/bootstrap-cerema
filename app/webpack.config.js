const Encore = require('@symfony/webpack-encore');
const FileManagerPlugin = require('filemanager-webpack-plugin');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('cerema', './assets/cerema.js')
    .addEntry('cerema-batiment', './assets/cerema-batiment.js')
    .addEntry('cerema-environnement', './assets/cerema-environnement.js')
    .addEntry('cerema-expertise', './assets/cerema-expertise.js')
    .addEntry('cerema-infrastructure', './assets/cerema-infrastructure.js')
    .addEntry('cerema-mer', './assets/cerema-mer.js')
    .addEntry('cerema-mobilite', './assets/cerema-mobilite.js')

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    // .enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    // Pour l'export du Cerema, je ne veux pas les séparer !
    //.splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    // Je veux tout dans le même fichier
    //.enableSingleRuntimeChunk()
    .disableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())

    //Pas besoin puisque notre souhait est d'exporter un fichier js et un fichier app
    .enableVersioning(false)

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    /**
     * Allows you to configure the options passed to the terser-webpack-plugin.
     * A list of available options can be found at https://github.com/webpack-contrib/terser-webpack-plugin
     */
    // .configureTerserPlugin((options) => {
    //      options.terserOptions = {
    //          output: {
    //              comments: false,
    //          },
    //      }
    // })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()

    // useful when running inside a Virtual Machine
    .configureWatchOptions(function(watchOptions) {
        if (!Encore.isProduction()) {
            watchOptions.poll = 1000; //check if there are updated files each second
        }
    })

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    //À la fin du traitement, on copie les résultats dans le répertoire dist
    .addPlugin(new FileManagerPlugin({
        events: {
            onStart: {
                delete: [{
                    source: '../dist',
                    options: {
                        force: true,
                    },
                }],
            },
            onEnd: {
                copy: [
                    {source: 'public/build/cerema.js', destination: '../dist/cerema.js'},
                    {source: 'public/build/*.css', destination: '../dist/'},
                    {source: 'public/build/cerema.js.LICENSE.txt', destination: '../dist/cerema.js.LICENSE.txt'},
                ],
            }
        }
    }))
;

module.exports = Encore.getWebpackConfig();
