# Bootstrap Cerema

Feuilles de styles Bootstrap adaptées pour respecter les couleurs de la charte graphique du Cerema.

# Comment l'utiliser ?

Prérequis : Si vous êtes ici, je considère que vous savez ce que sont le HTML et le CSS et que vous avez déjà utilisé 
des feuilles de style et notamment [Bootstrap](https://getbootstrap.com/). Si ce n'est pas le cas, je vous invite d'abord à lire la documentation de
[Bootstrap](https://getbootstrap.com/docs/5.2/getting-started/introduction/).

Voici une [démonstration temporairement en ligne !](https://bootstrap-cerema.longitude-one.fr/cerema/)  

## Pour démarrer rapidement 
Télécharger tous les fichiers dans le répertoire [dist](./dist)

Insérez dans votre code HTML la feuille de style `cerema.css` et le javascript `cerema.js` :

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap aux couleurs du Cerema</title>
    <link href="dist/cerema.css">
  </head>
  <body>
    <h1 class="text-primary">Cerema</h1>
    <script src="dist/cerema.js"></script>
  </body>
</html>
```

Attention les deux lignes `<meta>` sont nécessaires au bon fonctionnement de la charte graphique.

## Cas particuliers : les domaines d'activités
Si vous publiez un site ayant trait au domaine "Mer et littoral", votre charte graphique ne doit pas se baser sur 
l'orange, mais sur un bleu clair. Pas de souci ! Remplacer simplement le fichier `cerema.css` par `cerema-mer.css`.

Le code votre page web devient alors :

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap aux couleurs du Cerema : Mer et littoral</title>
    <link href="dist/cerema-mer.css">
  </head>
  <body>
    <h1 class="text-primary">Cerema</h1>
    <script src="dist/cerema.js"></script>
  </body>
</html>
```
*Seule la ligne 6 a été modifiée.*

# Qu'est-ce que j'obtiens ?

* Le thème graphique de [Bootstrap 5.2](https://getbootstrap.com/docs/5.2/getting-started/introduction/) avec les couleurs du Cerema !
* Toutes les fonctionnalités de la dernière version de [Bootstrap](https://getbootstrap.com/docs/5.2/getting-started/introduction/) sont intégrées !
* Les couleurs basiques de Bootstrap ont été remplacées pour être en accord avec la charte graphique du Cerema.
* Les couleurs des domaines spécifiques du Cerema sont intégrables en remplaçant la feuille de style initiale `cerema.css` par celle dédiée à votre domaine d'activité :
  * Bâtiment : `cerema-batiment.css` ;
  * Environnement : `cerema-environnement.css` ;
  * Expertise : `cerema-expertise.css` ;
  * Infrastructure : `cerema-infrastructure.css` ;
  * Mer et littoral : `cerema-mer.css` ;
  * Mobilité : `cerema-mobilite.css` (aucun changement par rapport à la charte initial du Cerema).
* Quel que soit le fichier utilisé, pour écrire un texte aux couleurs du Cerema:
  * text-cerema : écrit en orange cerema
  * text-cerema-100, text-cerema-200, text-cerema-300, text-cerema-400 : écrivent en orange avec un éclaircissement de 10%, 20%, ..., 90%
  * text-batiment : écrit en jaune du domaine Bâtiment (les éclaircissements existent aussi)
  * text-environnement : écrit en vert du domaine Environnement et risques (les éclaircissements existent aussi)
  * text-expertise : écrit en marine du domaine Expertise et ingénierie des territoires (les éclaircissements existent aussi)
  * text-infrastructure : écrit en pomme du domaine Infrastructure de transports (les éclaircissements existent aussi)
  * text-mer : écrit en orange du domaine Mer et littoral (les éclaircissements existent aussi)
  * text-mobilite : écrit en orange du domaine Mobilité (les éclaircissements existent aussi)
* Les suffixes ci-dessus fonctionnent aussi avec les boutons (btn-cerema), les bordures (border-mer), et tout ce qui existe dans bootstrap
* Les fichiers sont minifiés pour limiter la taille du téléchargement.
* Tous les composants de Bootstrap sont intégrés ET activés ! (Oui, y compris les popovers et les tooltips !)

# Qu'est-ce qu'il reste à faire ?

1. Intégrer la police Marianne ;
2. Intégrer la licence MIT dans le corps du javascript ;
3. Ajouter quelques captures d'écrans à la documentation ;
4. Créer un paquet NPM prêt à être déployé pour les développeurs sachant maitriser [SASS](https://sass-lang.com/) et [Node.js](https://nodejs.org/fr/) ;
5. Insérer le répertoire docker pour aider les contributeurs à déployer l'architecture ;

Si vous souhaitez contribuer, n'hésitez pas à lire la rubrique [CONTRIBUTE.md](CONTRIBUTE.md)
