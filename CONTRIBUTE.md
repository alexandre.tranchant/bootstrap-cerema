# Comment contribuer ?
Si vous êtes ici, c'est que vous voulez contribuer. Tout est codé en SASS comme Bootstrap. 
Je considère donc que vous savez installer les composants de base et contribuer à un projet open-source. 

Vous aurez besoin d'avoir installé cette stack sur votre PC :
* Git
* Yarn
* PHP 8
* Composer
* Symfony

(Si vous voulez j'ai un docker qui installe tout pour vous)

Pour installer le projet, clonez-le :
```bash
git clone https://gitlab.cerema.fr/alexandre.tranchant/bootstrap-cerema.git
cd bootstrap-cerema
```
Pour installer les outils node.js nécessaire à la compilation :
```bash
cd app
yarn install
```
Pour installer le serveur de page web et vérifier que vous n'avez pas tout péter :) :
```bash
composer install
symfony server:start -d
```
Compilez le sass !
```bash
yarn run encore prod
```
Maintenant, vous pouvez consulter la démo [http://127.0.0.1:8000](http://127.0.0.1:8000) sur votre navigateur.

Vous pouvez désormais modifier les fichiers sass qui se trouvent dans le répertoire app/assets.

Vous avez dû remarquer que la compilation de sass prenait beaucoup de temps, Symfony embarque un système qui met
à jour les fichiers chaque fois vous faites une modification.
```bash
yarn run encore dev watch
```
* Modifiez votre fichier
* Sauvegarder votre fichier
* La compilation rapide se lance automatiquement
* Consulter le site localement

Quand vous avez vérifier que tout fonctionne, faite un commit, et créer une Pull Request.